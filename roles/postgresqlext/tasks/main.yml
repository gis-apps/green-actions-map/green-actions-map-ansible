---

- name: Start and enable service postgresql
  systemd:
    name: postgresql@13-main
    state: started
    enabled: yes

- name: Add ubuntugis repository from PPA
  apt_repository:
    repo: ppa:ubuntugis/ubuntugis-unstable
    codename: focal # focal support still in ppa:ubuntugis/ubuntugis-unstable

- name: Install dependencies as packages
  when: install_dependencies_from_source != "yes"
  block:
    - name: Install dependencies UbuntuGIS
      apt:
        name:
          - "postgresql-13-pgrouting={{ pgrouting_package_version }}"
          - "postgresql-13-pgrouting-scripts={{ pgrouting_package_version }}"
          - "libgeos-3.9.0={{ geos_package_version }}"
          - "proj-bin={{ proj_package_version }}"
          - "proj-data={{ proj_package_version }}"
          - "gdal-bin={{ gdal_package_version }}"
          - "gdal-data={{ gdal_package_version }}"
          - "postgresql-13-postgis-3={{ postgis_package_version }}"
          - "postgresql-13-postgis-3-scripts={{ postgis_package_version }}"
        state: present
        update_cache: yes

- name: Install dependencies from source
  when: install_dependencies_from_source == "yes"
  block:
    - name: Install build dependencies
      package:
        name:
          - zip
          - unzip
          - build-essential
          - make
          - cmake
          - libboost-all-dev
          - libxml2
          - libjson-c-dev
          - sqlite3
          - libsqlite3-dev
          - libsqlite3-0
          - libtiff5
          - libtiff5-dev
          - libcurl4
          - libcurl4-openssl-dev
          - xsltproc
          - imagemagick
          - dblatex
          - docbook
          - docbook-xsl
          - libcunit1
          - libcunit1-dev
          - libxml2-dev
          - libgeos-dev
          - libgdal-dev
          - libprotobuf-c1
          - libprotobuf-c-dev
          - protobuf-c-compiler
        state: present
        update_cache: yes

    - name: Create a src directory
      file:
        path: /src/
        state: directory

    - name: Remove archive files and expanded folders from src folder
      file:
        path: "/src/{{ item }}"
        state: absent
      loop:
        - "pgrouting_v{{ pgrouting_source_version }}.tar.gz"
        - "pgrouting-{{ pgrouting_source_version }}/"
        - "geos-{{ geos_source_version }}.tar.bz2"
        - "geos-{{ geos_source_version }}/"
        - "proj-{{ proj_source_version }}.tar.gz"
        - "proj-{{ proj_source_version }}/"
        - "gdal-{{ gdal_source_version }}.tar.gz"
        - "gdal-{{ gdal_source_version }}/"
        - "postgis-{{ postgis_source_version }}.tar.gz"
        - "postgis-{{ postgis_source_version }}/"
        - "pgrouting*"
        - "geos*"
        - "proj*"
        - "postgis*"

    - name: Build pgrouting
      when: true
      block:
        - name: Download pgrouting
          get_url:
            url: "https://github.com/pgRouting/pgrouting/archive/v{{ pgrouting_source_version }}.tar.gz"
            dest: "/src/pgrouting_v{{ pgrouting_source_version }}.tar.gz"

        - name: Extract pgrouting archive into /src
          unarchive:
            src: "/src/pgrouting_v{{ pgrouting_source_version }}.tar.gz"
            dest: /src/
            remote_src: yes

        - name: Create a pgrouting build directory
          file:
            path: "/src/pgrouting-{{ pgrouting_source_version }}/build/"
            state: directory

        - name: Run command cmake on pgrouting
          command:
            cmd: cmake ..
            chdir: "/src/pgrouting-{{ pgrouting_source_version }}/build/"

        - name: Run command make on pgrouting
          command:
            cmd: make
            chdir: "/src/pgrouting-{{ pgrouting_source_version }}/build/"

        - name: Run command make install on pgrouting
          become: yes
          command:
            cmd: make install
            chdir: "/src/pgrouting-{{ pgrouting_source_version }}/build/"

    - name: Build geos
      when: true
      block:
        - name: Download geos
          get_url:
            url: "http://download.osgeo.org/geos/geos-{{ geos_source_version }}.tar.bz2"
            dest: "/src/geos-{{ geos_source_version }}.tar.bz2"

        - name: Extract geos archive into /src
          unarchive:
            src: "/src/geos-{{ geos_source_version }}.tar.bz2"
            dest: /src/
            remote_src: yes

        - name: Create a geos build directory
          file:
            path: "/src/geos-{{ geos_source_version }}/build/"
            state: directory

        - name: Run command cmake on geos
          command:
            cmd: cmake ..
            chdir: "/src/geos-{{ geos_source_version }}/build/"

    - name: Build proj
      when: true
      block:
        - name: Download proj
          get_url:
            url: "https://download.osgeo.org/proj/proj-{{ proj_source_version }}.tar.gz"
            dest: "/src/proj-{{ proj_source_version }}.tar.gz"

        - name: Extract proj archive into /src
          unarchive:
            src: "/src/proj-{{ proj_source_version }}.tar.gz"
            dest: /src/
            remote_src: yes

        - name: Create a proj build directory
          file:
            path: "/src/proj-{{ proj_source_version }}/build/"
            state: directory

        - name: Run command cmake on proj
          command:
            cmd: cmake ..
            chdir: "/src/proj-{{ proj_source_version }}/build/"

        - name: Run command cmake build on proj
          command:
            cmd: cmake --build .
            chdir: "/src/proj-{{ proj_source_version }}/build/"

        - name: Run command cmake build install on proj
          command:
            cmd: cmake --build . --target install
            chdir: "/src/proj-{{ proj_source_version }}/build/"

        - name: Run command projsync on proj
          command:
            cmd: projsync --system-directory --all
            chdir: "/src/proj-{{ proj_source_version }}/build/"

    - name: Build gdal
      when: true
      block:
        - name: Download gdal
          get_url:
            url: "https://github.com/OSGeo/gdal/releases/download/v{{ gdal_source_version }}/gdal-{{ gdal_source_version }}.tar.gz"
            dest: "/src/gdal-{{ gdal_source_version }}.tar.gz"

        - name: Extract gdal archive into /src
          unarchive:
            src: "/src/gdal-{{ gdal_source_version }}.tar.gz"
            dest: /src/
            remote_src: yes

        - name: Run command configure on gdal
          command:
            cmd: ./configure
            chdir: "/src/gdal-{{ gdal_source_version }}/"

        - name: Run command make on gdal
          command:
            cmd: make -j8 -s
            chdir: "/src/gdal-{{ gdal_source_version }}/"

        - name: Run command make on gdal apps
          command:
            cmd: make -s test_ogrsf
            chdir: "/src/gdal-{{ gdal_source_version }}/apps/"

        - name: Run command setdevenv on gdal
          become: yes
          command:
            cmd: bash setdevenv.sh
            chdir: "/src/gdal-{{ gdal_source_version }}/scripts/"

    - name: Build postgis
      when: true
      block:
        - name: Download postgis
          get_url:
            url: "https://download.osgeo.org/postgis/source/postgis-{{ postgis_source_version }}.tar.gz"
            dest: "/src/postgis-{{ postgis_source_version }}.tar.gz"

        - name: Extract postgis archive into /src
          unarchive:
            src: "/src/postgis-{{ postgis_source_version }}.tar.gz"
            dest: /src/
            remote_src: yes

        - name: Run command configure on postgis
          command:
            cmd: ./configure
            chdir: "/src/postgis-{{ postgis_source_version }}/"

        - name: Run command make on postgis
          command:
            cmd: make
            chdir: "/src/postgis-{{ postgis_source_version }}/"

        - name: Run command make install on postgis
          become: yes
          command:
            cmd: make install
            chdir: "/src/postgis-{{ postgis_source_version }}/"

- name: Create database and extensions
  when: false
  block:
    - name: Check whether database routing exists
      command:
        cmd: psql -h 127.0.0.1 -p 5432 -U postgres -w routing
      register: databases
      ignore_errors: yes

    - name: Run command add SUPERUSER privileges
      command:
        cmd: psql -h 127.0.0.1 -p 5432 -U postgres postgres -c 'ALTER USER green_actions_map_webapp_database_user WITH SUPERUSER;'

    - name: Run command createdb routing
      become: yes
      command:
        cmd: createdb -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing
      when: databases.stderr != ""

    - name: Run command create extension PostGIS
      command:
        cmd: psql -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing -c 'CREATE EXTENSION PostGIS'

    - name: Run command create extension pgRouting
      command:
        cmd: psql -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing -c 'CREATE EXTENSION pgRouting'

...
